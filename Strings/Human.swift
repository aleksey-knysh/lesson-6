//
//  Human.swift
//  Strings
//
//  Created by Aleksey Knysh on 12/31/21.
//

import Foundation

struct Human {
    let name: String
    let secondName: String?
    let surname: String
    let patronymic: String?
    let yearOld: String
    let id: String = UUID().uuidString
    var fullNameAndSecondName: String {
        """
        ФИО и второе имя
        ----------------
        фамилия \(surname)
        отчество: \(patronymic ?? "отчества нет")
        имя: \(name)
        псевдоним \(secondName ?? "второго имени нет")

        """
    }
    var fulNameAndAge: String {
        """

        ФИО и возраст
        -------------
        фамилия \(surname)
        отчество: \(patronymic ?? "отчества нет")
        имя: \(name)
        возраст \(yearOld)

        """
    }
    var fulName: String {
        "\(surname) \(name) \(patronymic ?? "отчества нет")"
    }
}
