//
//  ViewController.swift
//  Strings
//
//  Created by Aleksey Knysh on 12/20/21.
//

import UIKit

class ViewController: UIViewController {

    let firstPerson = Human(name: "Tania",
                            secondName: "Taniysha",
                            surname: "Strokava",
                            patronymic: "Aleksandrova",
                            yearOld: "25")

    let secondPerson = Human(name: "Vasia",
                             secondName: "Transformer",
                             surname: "Optimus",
                             patronymic: "Zaitsen",
                             yearOld: "30")

    let thirdPerson = Human(name: "Valia",
                            secondName: "Aftoritet",
                            surname: "Kokan",
                            patronymic: "Aleksandrovich",
                            yearOld: "23")

    let fourthPerson = Human(name: "Senia",
                             secondName: "Bobr",
                             surname: "luthi",
                             patronymic: "Aleksandrovich",
                             yearOld: "20")

    let fifthPerson = Human(name: "Igor",
                            secondName: "Babka",
                            surname: "Pops",
                            patronymic: "Bashkirovich",
                            yearOld: "18")

    let sixthPerson = Human(name: "Arina",
                            secondName: "Modnithsa",
                            surname: "Strokava",
                            patronymic: "Pahutina",
                            yearOld: "29")

    let seventhPerson = Human(name: "Sofia",
                              secondName: "Sofa",
                              surname: "Romanovskaia",
                              patronymic: "Adamovna",
                              yearOld: "34")

    let eighthPerson = Human(name: "Ruslan",
                             secondName: "Rusel",
                             surname: "Solomin",
                             patronymic: "Filipov",
                             yearOld: "32")

    let ninhtPerson = Human(name: "Kostia",
                            secondName: "Kostian",
                            surname: "Tolstyi",
                            patronymic: "Arkadevich",
                            yearOld: "24")

    let tenthPerson = Human(name: "Roman",
                            secondName: "Man",
                            surname: "Abramovich",
                            patronymic: "Filipov",
                            yearOld: "19")

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        let arrayOfPerson: [Human] = [firstPerson,
                                      secondPerson,
                                      thirdPerson,
                                      fourthPerson,
                                      fifthPerson,
                                      sixthPerson,
                                      seventhPerson,
                                      eighthPerson,
                                      ninhtPerson,
                                      tenthPerson]

        for man in arrayOfPerson {
            print(man.fullNameAndSecondName, man.fulNameAndAge)

            for compare in arrayOfPerson {
                guard man.id != compare.id else { continue }
                if man.surname == compare.surname || man.patronymic == compare.patronymic {
                    print("\(man.name) и \(compare.name) - родственики\n\n")
                }

                if compare.fulName.hasPrefix("R") {
                    print("Меняем R на S \(compare.fulName.replacingOccurrences(of: "R", with: "S"))\n\n")
                }

                var uperRemove = compare.fulName
                uperRemove.removeAll { $0.isUppercase }
                print("Удаляем заглавные буквы в \"ФИО\"\n--------------------------\n\(uperRemove)\n\n")

            }

            print("\nВыводим ФИО в верхнем регистре \(man.fulName.localizedUppercase)\n")
            print("\nВыводим ФИО в нижнем регистре \(man.fulName.localizedLowercase)\n\n")
        }
    }
}
